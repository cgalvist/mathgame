﻿using UnityEngine;
using System.Collections;
using Assets.Resources.Scripts;
using UnityEngine.UI;
using System.Collections.Generic;

public class GeneralScript : MonoBehaviour
{

	// ruta de los archivos JSON
	public static string dataURL = "Data/";

    // ruta del back end
    public static string backEndUrl = "http://mathbackgame.appspot.com/";

    // obtener header para peticion al back end
    public static Dictionary<string, string> getPostHeader()
    {
        Dictionary<string, string> postHeader = new Dictionary<string, string>();
        postHeader.Add("Content-Type", "application/json");
        return postHeader;
    }

    // convertir JSON a byte
    public static byte[] castJsonToByte(string json)
    {
        return System.Text.Encoding.UTF8.GetBytes(json);
    }

    // enviar peticion al back end
    public static IEnumerator waitForRequest(WWW data)
    {
        yield return data; // Wait until the download is done
        if (data.error != null)
        {
            Debug.Log("Error al enviar peticion: " + data.error);
        }
        else
        {
            Debug.Log("Envio exitoso: " + data.text);
        }
    }

    /* TEST 1 Y 3 */

    //colores de las celdas
    public static Color32 dark = new Color32(225, 153, 103, 255);
    public static Color32 light = new Color32(255, 255, 255, 255);

    //inicializar tableros
    public static void initBoards(Table[] tables, GameObject boardList, GameObject board, GameObject cell)
    {
		for (int i = 0; i < tables.Length; i++)
        {
            GameObject boardClone = Instantiate(board) as GameObject;
            boardClone.transform.SetParent(boardList.transform);

            //agregar celdas en los tableros
            for (int j = 0; j < tables[i].table.Length; j++)
            {
                GameObject cellClone = Instantiate(cell) as GameObject;
                cellClone.transform.SetParent(boardClone.transform);

                Image imageCellClone = cellClone.GetComponent<Image>();
				imageCellClone.color = (tables [i].table [j] == 0) ? light : dark;
            }
        }
    }

    //redimensionar celdas de uno o varios tableros
    public static IEnumerator resizeCells(GameObject boardContainer, string parentName)
    {
        Transform[] boardList2 = boardContainer.GetComponentsInChildren<Transform>();

        int xSize;
        float parentXSize;

        foreach (Transform t in boardList2)
        {
            yield return 0;
            if (t.parent.name == parentName)
            {
                RectTransform parent = t.GetComponent<RectTransform>();
                GridLayoutGroup grid = t.GetComponent<GridLayoutGroup>();
                LayoutElement boardSize = t.GetComponent<LayoutElement>();

                //igualar alto y ancho del tablero
                boardSize.preferredWidth = parent.rect.height;

                //escalar si el alto del contenedor es mayor que el ancho de la pantalla (para el tablero principal)
                if (parent.rect.height > Screen.width)
                {
                    float proportion = parent.rect.height / Screen.width;
                    float scale = (1 / proportion) - 0.01f;
                    parent.transform.localScale = new Vector3(scale, scale, 1);
                }

                xSize = (int)System.Math.Sqrt(t.childCount);
                parentXSize = parent.rect.height / xSize;
                //print("parent width and height: " + parent.rect.width + ", " + parent.rect.height);
                grid.cellSize = new Vector2(parentXSize, parentXSize);
            }
        }
    }

	//infomacion de los niveles del test 1
	public static LevelsT1Collection levelsT1;
	public static void initLevelsT1()
	{
		string test1DataURL = dataURL + "Test1/successions";
		TextAsset targetFile = Resources.Load<TextAsset>(test1DataURL);
		levelsT1 = JsonUtility.FromJson<LevelsT1Collection>(targetFile.text);
    }

    //infomacion de los niveles del test 3
    public static LevelsT3Collection levelsT3;
    public static void initLevelsT3()
    {
        string test3DataURL = dataURL + "Test3/successions";
        TextAsset targetFile = Resources.Load<TextAsset>(test3DataURL);
        levelsT3 = JsonUtility.FromJson<LevelsT3Collection>(targetFile.text);
    }

    /* TEST 5 */

    //infomacion de los niveles del test
    public static LevelsT5Collection levelsT5;
    public static void initLevelsT5()
    {
        string test5DataURL = dataURL + "Test5/successions";
        TextAsset targetFile = Resources.Load<TextAsset>(test5DataURL);
        levelsT5 = JsonUtility.FromJson<LevelsT5Collection>(targetFile.text);
    }

    //inicializar figuras
    public static void initFigures(int[] figures, GameObject boardList, GameObject board, GameObject figure)
    {
        for (int i = 0; i < figures.Length; i++)
        {
            GameObject containerClone = Instantiate(board) as GameObject;
            containerClone.transform.SetParent(boardList.transform);

            GameObject figureClone = Instantiate(figure) as GameObject;
            figureClone.transform.SetParent(containerClone.transform);

            Image imageFigureClone = figureClone.GetComponent<Image>();
            imageFigureClone.sprite = Resources.Load<Sprite>("Images/polygons/" + figures[i]);
        }
    }

    //inicializar figuras de la lista de respuestas
    public static void initFigures2(Answer[] answers, GameObject polygonList, GameObject polygonContainer, GameObject figure)
    {
        for (int i = 0; i < answers.Length; i++)
        {
            GameObject containerClone = Instantiate(polygonContainer) as GameObject;
            containerClone.name = answers[i].figure.ToString();

            //configurar para "drag & drop"
            containerClone.AddComponent<Draggable>();

            //agregar etiqueta para respuesta
            containerClone.transform.SetParent(polygonList.transform);

            GameObject figureClone = Instantiate(figure) as GameObject;
            figureClone.transform.SetParent(containerClone.transform);

            Image imageFigureClone = figureClone.GetComponent<Image>();
            imageFigureClone.sprite = Resources.Load<Sprite>("Images/polygons/" + answers[i].figure);
        }
    }

    //inicializar posiciones de la lista de respuestas
    public static void initPositions(int[] range, GameObject polygonList, GameObject polygonContainer, GameObject position)
    {
        for (int i = range[0]; i <= range[1]; i++)
        {
            GameObject containerClone = Instantiate(polygonContainer) as GameObject;
            containerClone.name = i.ToString();
            //configurar para "drag & drop"
            containerClone.AddComponent<DropZone>();
            containerClone.transform.SetParent(polygonList.transform);

            GameObject positionClone = Instantiate(position) as GameObject;
            positionClone.transform.SetParent(containerClone.transform);

            Text positionTextClone = positionClone.GetComponent<Text>();
            positionTextClone.text = i.ToString();
            
        }
    }

    /* TEST 6 */

    //infomacion de los niveles del test
    public static LevelsT6Collection levelsT6;
    public static void initLevelsT6()
    {
        string test6DataURL = dataURL + "Test6/successions";
        TextAsset targetFile = Resources.Load<TextAsset>(test6DataURL);
        levelsT6 = JsonUtility.FromJson<LevelsT6Collection>(targetFile.text);
    }

    /* TEST 7 */

    //infomacion de los niveles del test
    public static LevelsT7Collection levelsT7;
    public static void initLevelsT7()
    {
        string test7DataURL = dataURL + "Test7/successions";
        TextAsset targetFile = Resources.Load<TextAsset>(test7DataURL);
        levelsT7 = JsonUtility.FromJson<LevelsT7Collection>(targetFile.text);
    }
}
