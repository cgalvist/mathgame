﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    //we make static so in games only one script is name as this
    public static GameManager singleton;

    private AudioSource bgMusic;

    //it is call only once in a scene
    void Awake()
    {
        MakeSingleton();
    }

    void MakeSingleton()
    {
        if (singleton != null)
        {
            Destroy(gameObject);
        }
        else
        {
            singleton = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    void Start()
    {
        bgMusic = GetComponent<AudioSource>();
    }

    void Update()
    {
        // desactivar sonidos en niveles del juego
        string levelName = SceneManager.GetActiveScene().name;
        if (levelName.Substring(0,4) == "test")
        {
            bgMusic.Stop();
        } else
        {
            if (!bgMusic.isPlaying)
            {
                bgMusic.Play();
            }
        }
    }

}