﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class test2MenuScript : MonoBehaviour {

    public Text Titulo;
    public Button menu;
    public Button siguiente;
    public Button reiniciar;
    public GameObject esfera;
    public Text error;
    public Text acierto;

    public void menuButton()
    {
        SceneManager.LoadScene("category1");
    }

    public void siguienteButton()
    {
        desactivarBotones();
        desactivarMensajes();
        SphereMove.movimiento = true;
        esfera.transform.position = new Vector3(0,0.945f,0);
        Test2Script.iniciar(Titulo,Test2Script.nivelCont<9? Test2Script.nivelCont + 1: 0);
    }

    public void reiniciarButton()
    {
        desactivarBotones();
        desactivarMensajes();
        SphereMove.movimiento = true;
        esfera.transform.position = new Vector3(0, 0.945f, 0);
        Test2Script.iniciar(Titulo, Test2Script.nivelCont);
    }

    public void desactivarBotones()
    {
        menu.gameObject.SetActive(false);
        siguiente.gameObject.SetActive(false);
        reiniciar.gameObject.SetActive(false);
    }

    public void desactivarMensajes()
    {
        error.enabled = false;
        acierto.enabled = false;
    }
}
