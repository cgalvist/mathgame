﻿using UnityEngine;
using System.Collections;
using Assets.Resources.Scripts.Model;

public class MovimientoBurbuja : MonoBehaviour {

    public float motionAngle = 0.0f;
    public float originalXPos, originalYPos,dato;
    public float DatX=2.0f, DatY=150.0f, DatZ=-1.0f;
    Vector3 driftVelocity;

    void Start()
    {
        transform.transform.position = new Vector3(Random.Range(-2.5f, 2.5f), Random.Range(6.0f, 12.0f),0f);
        DatX = Random.Range(1.5f, 2.0f);
        DatY = Random.Range(100f, 150f);
        originalXPos = transform.position.x;
        originalYPos = transform.position.y;
    }
   
    void Update () {

        if (Test4Script.estado.Equals("inGame"))
        {
            if (transform.position.y <= -6.3)
            {
                DatX = Random.Range(1.5f, 2.0f);
                DatY = Random.Range(100f, 150f);
                transform.transform.position = new Vector3(Random.Range(-2.5f, 2.5f), Random.Range(6.0f, 12.0f), 0f);
            }
            else
            {
                driftVelocity = GetBubbleMotion(DatX, DatY, DatZ);
                // include current Y position to increment gravity. X position must not increment. Use original X if required (as shown above).
                Vector3 bubblePos = new Vector3(originalXPos, transform.position.y, 0.0f);
                transform.position = bubblePos + driftVelocity * Time.deltaTime;
            }
            dato = transform.position.y;
        }
    }

    private Vector3 GetBubbleMotion(float motionSpeed, float motionWidth, float motionGravity)
    {
        motionAngle += Mathf.PI / 180.0f * motionSpeed;
        return new Vector3(Mathf.Sin(motionAngle) * motionWidth, motionGravity, 0.0f);
    }
}
