﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Assets.Resources.Scripts.Model;
using UnityEngine.UI;
using System.Collections.Generic;

public class ButtonsT4Script : MonoBehaviour {

    public static int idOpcion;
    public Text testText;
    public Text errorText;
    public Text correctText;
    public Button siguiente;
    public Button reintentar;
    public Button menu;
    public Image imagenTitulo;

    public List<GameObject> burbujas;

    public List<GameObject> figuras;

    public List<Sprite> spritesEj1;
    public List<Sprite> spritesEj2;
    public List<Sprite> spritesEj3;
    public List<Sprite> spritesEj4;
    public List<Sprite> spritesEj5;
    public List<Sprite> spritesEj6;
    public List<Sprite> spritesEj7;
    public List<Sprite> spritesEj8;
    public List<Sprite> spritesEj9;
    public List<Sprite> spritesEj10;

    public List<Sprite> spritesTituloEj;

    private int randNumSprite;
    private int contFig;

    private void Start()
    {
        idOpcion = -1;
        contFig = 0;

        iniciar();
    }

    public void accionBoton(int id)
    {
        idOpcion = id;
        Test4Script.estado = "paused";
        reiniciarPosBurbujas();

        EjercicioT4 nivel = (EjercicioT4)Test4Script.Niveles[Test4Script.nivelActual];

        if (idOpcion == nivel.Respuesta)
        {
            correctText.gameObject.SetActive(true);
            siguiente.gameObject.SetActive(true);
            errorText.gameObject.SetActive(false);
            reintentar.gameObject.SetActive(false);
            guardarPuntaje(true);
        }
        else
        {
            errorText.gameObject.SetActive(true);
            reintentar.gameObject.SetActive(true);
            correctText.gameObject.SetActive(false);
            siguiente.gameObject.SetActive(false);
            guardarPuntaje(false);
        }

        menu.gameObject.SetActive(true);
    }

    public void botonSalir()
    {
        SceneManager.LoadScene("category1");
    }

    public void reiniciar()
    {
        reiniciarPosBurbujas();

        Test4Script.estado = "inGame";

        reiniciarMenus();
    }

    public void siguienteNivel()
    {
        Test4Script.nivelActual++;

        if (Test4Script.nivelActual >= 10)
        {
            Test4Script.nivelActual--;
        }

        EjercicioT4 nivel = (EjercicioT4)Test4Script.Niveles[Test4Script.nivelActual];
        testText.text = nivel.Texto;

        cambiarImagentitulo();
        reiniciarPosBurbujas();
        cambiarImagenBurbujas();

        Test4Script.estado = "inGame";

        reiniciarMenus();
    }

    public void iniciar()
    {
        Test4Script.nivelActual = 0;
        EjercicioT4 nivel = (EjercicioT4)Test4Script.Niveles[Test4Script.nivelActual];
        testText.text = nivel.Texto;

        cambiarImagentitulo();
        reiniciarPosBurbujas();
        cambiarImagenBurbujas();
    }

    public void reiniciarMenus()
    {
        errorText.gameObject.SetActive(false);
        correctText.gameObject.SetActive(false);
        siguiente.gameObject.SetActive(false);
        reintentar.gameObject.SetActive(false);
        menu.gameObject.SetActive(false);
    }

    public void reiniciarPosBurbujas()
    {
        burbujas[0].transform.localPosition = new Vector3(192f, 1263f, 711.66f);
        burbujas[1].transform.localPosition = new Vector3(-252f, 1082f, 711.66f);
        burbujas[2].transform.localPosition = new Vector3(-221f, 896f, 711.66f);
        burbujas[3].transform.localPosition = new Vector3(-241f, 1378f, 711.66f);
        burbujas[4].transform.localPosition = new Vector3(221f, 810f, 711.66f);
        burbujas[5].transform.localPosition = new Vector3(0f, 1575.6f, 711.66f);
    } 

    public void cambiarImagentitulo()
    {
        imagenTitulo.sprite = spritesTituloEj[Test4Script.nivelActual];
    }

    public void cambiarImagenBurbujas()
    {
        EjercicioT4 nivel = (EjercicioT4)Test4Script.Niveles[Test4Script.nivelActual];

        (figuras[nivel.Respuesta - 1].GetComponent(typeof(SpriteRenderer))
            as SpriteRenderer).sprite =  getImagenesBurbujas()[nivel.Respuesta - 1];

        contFig = 0;
        while(contFig<6)
        {
            randNumSprite = Random.Range(0, (nivel.Respuesta - 1) < 6 ? 4 : 5);

            if (contFig == (nivel.Respuesta - 1))
            {
                contFig++;
            }

            if (randNumSprite != (nivel.Respuesta - 1)  && contFig < 6)
            {
                (figuras[contFig].GetComponent(typeof(SpriteRenderer))
                    as SpriteRenderer).sprite = getImagenesBurbujas()[randNumSprite];
                contFig++;
            }

        }
    }

    public List<Sprite> getImagenesBurbujas()
    {
        List<Sprite> sprites = null;

        switch (Test4Script.nivelActual)
        {
            case 0:
                sprites = spritesEj1;
                break;
            case 1:
                sprites = spritesEj2;
                break;
            case 2:
                sprites = spritesEj3;
                break;
            case 3:
                sprites = spritesEj4;
                break;
            case 4:
                sprites = spritesEj5;
                break;
            case 5:
                sprites = spritesEj6;
                break;
            case 6:
                sprites = spritesEj7;
                break;
            case 7:
                sprites = spritesEj8;
                break;
            case 8:
                sprites = spritesEj9;
                break;
            case 9:
                sprites = spritesEj10;
                break;
        }

        return sprites;
    }

    public void guardarPuntaje(bool correcto)
    {
        string usernameStr = PlayerPrefs.GetString("username");
        string json = "{\"username\":\"" + usernameStr + "\"," +
            "\"problem\":\"test 4 - Level " + (Test4Script.nivelActual + 1) + "\"," +
            "\"score\":\"" + (correcto ? 1 : 0) + "\"}";
        var formData = GeneralScript.castJsonToByte(json);
        Debug.Log("json: " + json);

        WWW www = new WWW(GeneralScript.backEndUrl + "score", formData, GeneralScript.getPostHeader());
        StartCoroutine(GeneralScript.waitForRequest(www));
    }

}
