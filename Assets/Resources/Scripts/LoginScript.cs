﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoginScript : MonoBehaviour {

    //objetos del nivel
    public Text username;

    public void acceptButton()
    {
        // leer nombre de usuario.
        // En caso de ser válido lo guarda y cambia a la vista de niveles
        if(username != null && username.text.Length > 0)
        {
            PlayerPrefs.SetString("username", username.text);
            //SceneManager.LoadScene("category1");

            //enviar datos al back end
            string usernameStr = username.text;
            Debug.Log("username: " + usernameStr);

            // convertir string JSON a byte
            var formData = GeneralScript.castJsonToByte("{\"username\":\"" + usernameStr + "\"}");

            WWW www = new WWW(GeneralScript.backEndUrl + "ingress", formData, GeneralScript.getPostHeader());
            StartCoroutine(WaitForRequest(www));
        }

    }

    // enviar peticion al back end
    IEnumerator WaitForRequest(WWW data)
    {
        yield return data; // Wait until the download is done
        if (data.error != null)
        {
            Debug.Log("Error al enviar peticion: " + data.error);
        }
        else
        {
            Debug.Log("Envio exitoso: " + data.text);
            SceneManager.LoadScene("category1");
        }
    }
}
