﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Collition : MonoBehaviour {

    public int valor;
    public Text txtCorrecto;
    public Text txtError;
    public TextMesh textMesh;
    public Button menu;
    public Button siguiente;
    public Button reintentar;
    public GameObject esfera;

    private void Start()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if ( !textMesh.text.Equals(Test2Script.respuesta.ToString()) )
        {
            txtError.enabled = true;
            txtCorrecto.enabled = false;
            reintentar.gameObject.SetActive(true);
            guardarPuntaje(false);
        }
        else
        {
            txtCorrecto.enabled = true;
            txtError.enabled = false;
            siguiente.gameObject.SetActive(true);
            guardarPuntaje(true);
        }
        menu.gameObject.SetActive(true);

        SphereMove.movimiento = false;

    }

    public void guardarPuntaje(bool correcto)
    {
        string usernameStr = PlayerPrefs.GetString("username");
        string json = "{\"username\":\"" + usernameStr + "\"," +
            "\"problem\":\"test 2 - Level " + (Test2Script.nivelCont + 1) + "\"," +
            "\"score\":\"" + (correcto ? 1 : 0) + "\"}";
        var formData = GeneralScript.castJsonToByte(json);
        Debug.Log("json: " + json);

        WWW www = new WWW(GeneralScript.backEndUrl + "score", formData, GeneralScript.getPostHeader());
        StartCoroutine(GeneralScript.waitForRequest(www));
    }

}
