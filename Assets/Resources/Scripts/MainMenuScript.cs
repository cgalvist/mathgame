﻿using UnityEngine;
using UnityEngine.SceneManagement;

//clase para manejar cambios de escenas en el juego
public class MainMenuScript : MonoBehaviour {

    public void menuButton()
    {
        SceneManager.LoadScene("menu");
    }

    public void optionsButton()
    {
        SceneManager.LoadScene("options");
    }

    public void exit()
    {
        Application.Quit();
    }

    /* TESTS */

    public void test1Button()
    {
        SceneManager.LoadScene("test1");
    }

    public void test2Button()
    {
        SceneManager.LoadScene("test2");
    }

    public void test3Button()
    {
        SceneManager.LoadScene("test3");
    }

    public void test4Button()
    {
        SceneManager.LoadScene("test4");
    }

    public void test5Button()
    {
        SceneManager.LoadScene("test5");
    }

    public void test6Button()
    {
        SceneManager.LoadScene("test6");
    }

    public void test7Button()
    {
        SceneManager.LoadScene("test7");
    }

    /* CATEGORIAS */

    public void categoriesButton()
    {
        SceneManager.LoadScene("categoriesMenu");
    }

    public void category1Button()
    {
        SceneManager.LoadScene("category1");
    }

    public void restartScene()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

}
