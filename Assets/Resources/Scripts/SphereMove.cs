﻿using UnityEngine;
using System.Collections;

public class SphereMove : MonoBehaviour {

    Rigidbody rb;
    Vector3 gravity;
    float g = 9.8f;
    public static bool movimiento;
    private Vector3 vec, vec0;

    // Use this for initialization
    void Start () {
        movimiento = true;
        rb = GetComponent<Rigidbody>();
        vec0 = new Vector3(0,0,0);
	}
	
	// Update is called once per frame
	void Update () {

        if (movimiento)
        {
            vec.x = Input.acceleration.x;
            vec.y = Input.acceleration.z;
            vec.z = Input.acceleration.y;

            gravity = vec * g;
            rb.AddForce(gravity, ForceMode.Acceleration);
        }else
        {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
        }
        
    }
}
