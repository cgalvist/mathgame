﻿using System;

//clases para manejar la conversion de datos en formato JSON del nivel 6
namespace Assets.Resources.Scripts
{
    [Serializable]
    public class LevelT6
    {
        public int[] figures;
        public string[] answer;
        public string[] options;

    }

    [Serializable]
    public class LevelsT6Collection
    {
        public LevelT6[] levels;
    }
}