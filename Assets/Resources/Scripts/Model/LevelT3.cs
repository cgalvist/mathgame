﻿using System;

//clases para manejar la conversion de datos en formato JSON del nivel 3
namespace Assets.Resources.Scripts 
{ 
	[Serializable]
	public class LevelT3
	{ 
		public Table[] tables;
		public string answer;

	}

	[Serializable]
	public class LevelsT3Collection{
		public LevelT3[] levels;
	}
}