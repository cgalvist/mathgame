﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Resources.Scripts.Model
{
    class EjercicioT4
    {
        public String Texto;
        public int Respuesta;

        public EjercicioT4(String texto, int respuesta)
        {
            this.Texto = texto;
            this.Respuesta = respuesta;
        }
    }
}
