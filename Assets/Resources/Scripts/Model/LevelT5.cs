﻿using System;

//clases para manejar la conversion de datos en formato JSON del nivel 5
namespace Assets.Resources.Scripts
{
    [Serializable]
    public class LevelT5
    {
        public int[] figures;
        public Answer[] answers;
        public int[] range;

    }

    [Serializable]
    public class LevelsT5Collection
    {
        public LevelT5[] levels;
    }

    [Serializable]
    public class Answer
    {
        public int figure;
        public int position;

    }
}