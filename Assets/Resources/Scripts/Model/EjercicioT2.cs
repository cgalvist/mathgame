﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Resources.Scripts
{
    class EjercicioT2
    {
        public String Texto;
        public int Respuesta;

        public EjercicioT2(String texto, int respuesta)
        {
            this.Texto = texto;
            this.Respuesta = respuesta;
        }

    }
}
