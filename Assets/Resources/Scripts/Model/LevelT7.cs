﻿using System;

//clases para manejar la conversion de datos en formato JSON del nivel 7
namespace Assets.Resources.Scripts
{
    [Serializable]
    public class LevelT7
    {
        public FigureT7[] figures;
        public string text;
        public FigureT7 answer;

    }

    [Serializable]
    public class LevelsT7Collection
    {
        public LevelT7[] levels;
    }

    [Serializable]
    public class FigureT7
    {
        public int figure;
        public string color;
    }
}