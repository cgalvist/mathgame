﻿using System;

//clases para manejar la conversion de datos en formato JSON del nivel 1
namespace Assets.Resources.Scripts 
{ 
	[Serializable]
	public class LevelT1
	{ 
		public Table[] tables;
		public int[] answer;

	}

	[Serializable]
	public class LevelsT1Collection{
		public LevelT1[] levels;
	}
}