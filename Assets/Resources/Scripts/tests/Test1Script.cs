﻿using UnityEngine;
using System;
using System.Linq;
using Assets.Resources.Scripts;
using UnityEngine.UI;

public class Test1Script : MonoBehaviour
{
	//nivel de dificultad del juego
	private int level = 0;

	//datos del nivel
	private LevelT1 levelInfo;

    //objetos del nivel
    public GameObject boardList;
    public GameObject mainBoard;
    public GameObject mainBoardContainer;
    public GameObject board;
    public GameObject cell;

    //pop ups del nivel
    public GameObject popupSuccess;
	public GameObject popupMessage;
	public Text popupText;

    // Use this for initialization
    void Start()
	{

		//verificar nivel de dificultad del juego
		//PlayerPrefs.SetInt ("test1_level", 0);
		if (PlayerPrefs.GetInt("test1_level") >= 0) {
			level = PlayerPrefs.GetInt("test1_level");
		} else {
			PlayerPrefs.SetInt("test1_level", 0);
		}
		//print("Nivel actual: " + level);

		//inicializar datos de los niveles del test
		GeneralScript.initLevelsT1();
		levelInfo = (LevelT1) GeneralScript.levelsT1.levels[level];

        //inicializar tableros
		GeneralScript.initBoards(levelInfo.tables,boardList,board,cell);
        initMainBoard();
        StartCoroutine(GeneralScript.resizeCells(boardList, "SuccessionsGrid"));
        StartCoroutine(GeneralScript.resizeCells(mainBoardContainer, "MainBoardContainer"));
    }

    //inicializar tablero principal
    public void initMainBoard()
    {
        //int cellNum = (int)System.Math.Pow(mainXSize, 2);
		int cellNum = levelInfo.answer.Length;

        //agregar celdas en los tableros
        for (int j = 0; j < cellNum; j++)
        {
            GameObject cellClone = Instantiate(cell) as GameObject;
            cellClone.name = "mbc" + j;

            //agregar componente "button" y configurar clic
            cellClone.AddComponent<Button>();
            Button buttonCellClone = cellClone.GetComponent<Button>();
            buttonCellClone.onClick.AddListener(delegate { checkCell(cellClone); });
            
            cellClone.transform.SetParent(mainBoard.transform);
        }
    }

    //funcion al tocar una celda del tablero principal
    public void checkCell(GameObject cellClone)
    {
        //print(cellClone.name);
        Image imageCellClone = cellClone.GetComponent<Image>();

        if (imageCellClone.color == GeneralScript.light)
        {
            imageCellClone.color = GeneralScript.dark;
        } else
        {
            imageCellClone.color = GeneralScript.light;
        }
    }

    //limpiar tablero principal
    public void clearMainBoard()
    {
        Transform[] mainCells = mainBoard.GetComponentsInChildren<Transform>();

        foreach (Transform child in mainCells)
        {
            if (child.parent.name == "board")
            {
                Image imageCellClone = child.GetComponent<Image>();
                imageCellClone.color = GeneralScript.light;
            }
        }
    }

    //leer tablero principal editado por el usuario
    public void readMainBoard()
    {
        Transform[] mainCells = mainBoard.GetComponentsInChildren<Transform>();
        string result = "";

        foreach (Transform child in mainCells)
        {
            if (child.parent.name == "board")
            {
                Image imageCellClone = child.GetComponent<Image>();

				result += (imageCellClone.color == GeneralScript.light) ? "0," : "1,";
            }
        }
		result = result.Substring (0, result.Length - 1);
        //print(result);
		int[] tempAnswer = result.Split(',').Select(n => Convert.ToInt32(n)).ToArray();

		Boolean correctAnswer = true;
		for (int i = 0; i < tempAnswer.Length; i++)
		{
			if (tempAnswer[i] != levelInfo.answer[i])
			{
				correctAnswer = false;
				break;
			}
        }

        // datos de usuario
        string usernameStr = PlayerPrefs.GetString("username");
        string json = "{\"username\":\"" + usernameStr + "\"," +
            "\"problem\":\"" + "test1 - level " + level + "\"," +
            "\"score\":\"" + (correctAnswer ? 1 : 0) + "\"}";
        var formData = GeneralScript.castJsonToByte(json);
        Debug.Log("json: " + json);

        // enviar datos al back end
        WWW www = new WWW(GeneralScript.backEndUrl + "score", formData, GeneralScript.getPostHeader());
        StartCoroutine(GeneralScript.waitForRequest(www));

        if (correctAnswer)
		{
			level++;

			if (level < GeneralScript.levelsT1.levels.Length){
				PlayerPrefs.SetInt("test1_level", level);
				popupSuccess.SetActive(true);
			} else {
				popupText.text = "Felicitaciones, ya has superado todos los niveles de esta prueba.";
				popupMessage.SetActive(true);
			}
		}
		else 
		{
			popupText.text = "Respuesta erronea. Intenta de nuevo.";
			popupMessage.SetActive(true);
		}
    }

}
