﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using NCalc;
using System;
using System.Text.RegularExpressions;
using Assets.Resources.Scripts;

public class Test3Script : MonoBehaviour
{
    //nivel de dificultad del juego
    private int level = 0;

    //datos del nivel
    private LevelT3 levelInfo;

    //tipo de expresion del juego
    private bool isAlgebraic = true;

    //lista de expresiones
    private ArrayList expressions;

    //pop ups del nivel
    public GameObject popupSuccess;
    public GameObject popupMessage;
    public Text popupText;
    public GameObject popupExpressions;
    public GameObject popupNumber;
    public GameObject popupVar;

    //objetos del nivel
    public GameObject boardList;
    public GameObject board;
    public GameObject cell;
    public GameObject mathOption;
    public GameObject input;
    public GameObject expressionList;

    //botones
    public GameObject selectExp;

    // Use this for initialization
    void Start()
    {

        //verificar nivel de dificultad del juego
        //PlayerPrefs.SetInt ("test3_level", 0);
        if (PlayerPrefs.GetInt("test3_level") >= 0)
        {
            level = PlayerPrefs.GetInt("test3_level");
        }
        else
        {
            PlayerPrefs.SetInt("test3_level", 0);
        }
        //print("Nivel actual: " + level);

        //inicializar datos de los niveles del test
        GeneralScript.initLevelsT3();
        levelInfo = (LevelT3) GeneralScript.levelsT3.levels[level];

        //inicializar lista de expresiones
        expressions = expressionsList();
        foreach (GameExpression expression in expressions)
        {
            GameObject listItem = Instantiate(mathOption) as GameObject;
            Button buttonItem = listItem.GetComponent<Button>();
            GameExpression tempExp = new GameExpression(expression.getAlgebraic(), expression.getVerbal());
            buttonItem.onClick.AddListener(delegate { addInputExpression(tempExp);});
            listItem.transform.SetParent(expressionList.transform);
        }

        //agregar evento para boton de seleccion de tipo de expresion
        Button buttonSelectExp = selectExp.GetComponent<Button>();
        buttonSelectExp.onClick.AddListener(delegate { changeExpression(); });

        //inicializar tableros
        GeneralScript.initBoards(levelInfo.tables,boardList,board,cell);
        StartCoroutine(GeneralScript.resizeCells(boardList, "SuccessionsGrid"));

        //drawOptions(0);
    }

    //limpiar cambios realizados por el usuario
    public void clear()
    {
        InputField inputExp = input.GetComponent<InputField>();
        inputExp.text = "";
    }

    //validar operacion del usuario
    public void send()
    {
        //leer datos
        InputField inputExp = input.GetComponent<InputField>();
        string stringExpression = inputExp.text;
        //print(stringExpression);

        //obtener componentes del popup
        Transform[] popupMessage2 = popupMessage.GetComponentsInChildren<Transform>();
        foreach (Transform t in popupMessage2)
        {
            if (t.parent.name == "PopupText")
                popupText = t.GetComponent<Text>();
        }

        try
        {
            ArrayList variables = new ArrayList();

            //leer el numero de variables de la expresion

            //si es verbal, cambiar los textos por expresiones algebraicas
            if (!isAlgebraic)
            {
                foreach (GameExpression gameExpression in expressions)
                {
                    stringExpression = stringExpression.Replace(gameExpression.getVerbal(),gameExpression.getAlgebraic());
                }
                //print(stringExpression);
            }

            /* 
             * cambiar texto para permitir potenciacion reemplazando la expresion "a^b"
             * por la expresion "Pow(a,b)"
             */

            //TODO: Es recomendable mejorar esta funcion usando expresiones regulares mas incluyentes o un parser
            if (stringExpression.Contains("^"))
            {
                string deleteParenthesisRegex = "(\\((\\d+|\\w)\\))";
                string detectPrimaryPowRegex = "(((\\d+|[A-Za-z])|(\\(([^()\\^]*)\\)))\\^((\\d+|[A-Za-z])|(\\(([^()\\^]*)\\))))(?!\\^)";
                string detectNestedPowRegex = "(((\\d+|[A-Za-z])|(\\(([^()]*)\\))))\\^(Pow\\([\\w\\+\\-*/()]+,([\\w\\+\\-*/()]+)\\))(?!\\^)";
                string loopPowString = "Pow\\([\\w\\+\\-*/()]+,([\\w\\+\\-*/()]+)\\)";
                string replaceString = "([\\w\\+\\-*/()]+)";

                bool flag = true;
                string temp;

                //eliminar parentesis innecesarios
                while (flag)
                {
                    flag = false;
                    foreach (Match match in Regex.Matches(stringExpression, deleteParenthesisRegex))
                    {
                        if (!flag) flag = true;
                        //Console.WriteLine("{0} (duplicates '{1}') at position {2}", match.Value, match.Value, match.Index);
                        temp = match.Value.Substring(1, match.Value.Length - 2);
                        stringExpression = stringExpression.Replace(match.Value, temp);
                    }
                }

                //cambiar exponentes principales
                foreach (Match match in Regex.Matches(stringExpression, detectPrimaryPowRegex))
                {
                    if (!flag) flag = true;
                    temp = "Pow(" + match.Value.Split('^')[0] + "," + match.Value.Split('^')[1] + ")";
                    stringExpression = stringExpression.Replace(match.Value, temp);
                }

                //cambiar exponentes anidados
                //NOTA: funciona maximo con 10 niveles de profundidad
                int count = 0;
                while(count < 10)
                {
                    foreach (Match match in Regex.Matches(stringExpression, detectNestedPowRegex))
                    {
                        if (!flag) flag = true;
                        temp = "Pow(" + match.Value.Split('^')[0] + "," + match.Value.Split('^')[1] + ")";
                        stringExpression = stringExpression.Replace(match.Value, temp);
                    }
                    detectNestedPowRegex = detectNestedPowRegex.Replace(replaceString,loopPowString);
                    count++;
                }

                //print(stringExpression);
            }

            foreach (char variable in stringExpression)
            {
                if(((variable >= 'a' && variable <= 'z') || (variable >= 'A' && variable <= 'Z'))
                    && !variables.Contains(variable))
                {
                    variables.Add(variable);
                }
            }

            //verificar expresion ingresada por el usuario

            //Expression expression = new Expression("(450*5)+((3.14*7)/50)*100");
            Expression expression = new Expression(stringExpression);
            Boolean correctAnswer = true;

            for (int i = 0; i < levelInfo.tables.Length; i++)
            {
                foreach (char variable in variables)
                {
                    //expression.Parameters[variable + ""] = 0;
                    expression.Parameters[variable + ""] = i + 1;
                }
                //print("expresion: " + expression.Evaluate());
                //print("JSON: " + numberCheckTable(levelInfo.tables[i]));
                if (expression.Evaluate().ToString() != numberCheckTable(levelInfo.tables[i]).ToString())
                {
                    correctAnswer = false;
                    break;
                }
            }

            // datos de usuario
            string usernameStr = PlayerPrefs.GetString("username");
            string json = "{\"username\":\"" + usernameStr + "\"," +
                "\"problem\":\"" + "test3 - level " + level + "\"," +
                "\"score\":\"" + (correctAnswer ? 1 : 0) + "\"}";
            var formData = GeneralScript.castJsonToByte(json);
            Debug.Log("json: " + json);

            // enviar datos al back end
            WWW www = new WWW(GeneralScript.backEndUrl + "score", formData, GeneralScript.getPostHeader());
            StartCoroutine(GeneralScript.waitForRequest(www));

            if (correctAnswer)
            {
                level++;

                if (level < GeneralScript.levelsT3.levels.Length)
                {
                    PlayerPrefs.SetInt("test3_level", level);
                    popupSuccess.SetActive(true);
                }
                else
                {
                    popupText.text = "Felicitaciones, ya has superado todos los niveles de esta prueba.";
                    popupMessage.SetActive(true);
                }
            }
            else
            {
                popupText.text = "Respuesta erronea. Intenta de nuevo.";
                popupMessage.SetActive(true);
            }         

        } catch(EvaluationException e)
        {
            popupText.text = "Error al evaluar la expresi\u00f3n";
            popupMessage.SetActive(true);
        }
        catch (ArgumentException e)
        {
            popupText.text = "Error al evaluar la expresi\u00f3n";
            popupMessage.SetActive(true);
        }

        //double result = (double)expression.Evaluate();
        //print("Resultado: " + result);
    }

    //cambiar el tipo de expresion del juego (algebraica o verbal)
    public void changeExpression()
    {
        isAlgebraic = !isAlgebraic;
        selectExp.GetComponentInChildren<Text>().text = (isAlgebraic ? " Algebr\u00e1ica " : "     Verbal     ");
        clear();
    }

    //agregar una expresion al juego
    public void addExpression()
    {
        Transform[] expressionsListArray = expressionList.GetComponentsInChildren<Transform>();        

        int count = 0;
        foreach (Transform child in expressionsListArray)
        {
            if (child.parent.name == "ExpressionsGrid")
            {
                GameExpression tempExpression = expressions[count] as GameExpression;
                child.GetComponentInChildren<Text>().text = (isAlgebraic ? tempExpression.getAlgebraic() : tempExpression.getVerbal());
                count++;
            }
        }
    }

    //funcion al dar clic en un item de la lista de expresiones
    public void addInputExpression(GameExpression expression)
    {
        popupExpressions.SetActive(false);
        //agregar datos
        InputField inputExp = input.GetComponent<InputField>();
        if(expression.getAlgebraic() == "n\u00famero")
            popupNumber.SetActive(true);
        else
        if (expression.getAlgebraic() == "variable")
            popupVar.SetActive(true);
        else
            inputExp.text += (isAlgebraic ? expression.getAlgebraic() : expression.getVerbal());
                
    }

    //funcion al agregar numero o variable desde el pop up
    public void addInputPopup(GameObject inputPopup)
    {
        InputField inputExp = input.GetComponent<InputField>();
        InputField inputFieldPopup = inputPopup.GetComponent<InputField>();
        inputExp.text += inputFieldPopup.text;
        inputFieldPopup.text = "";
        //print(inputExp.text);
    }

    //listas de operadores y opciones para expresiones verbales y algebraicas
    public class GameExpression
    {
        private string algebraic;
        private string verbal;

        public GameExpression(string algebraic, string verbal)
        {
            this.algebraic = algebraic;
            this.verbal = verbal;
        }

        public string getAlgebraic()
        {
            return algebraic;
        }

        public string getVerbal()
        {
            return verbal;
        }
    }

    //lista de expresiones verbales
    private ArrayList expressionsList()
    {
        ArrayList expressions = new ArrayList();

        expressions.Add(new GameExpression("n\u00famero", "n\u00famero"));
        expressions.Add(new GameExpression("variable", "variable"));
        expressions.Add(new GameExpression("+", " sumado con "));
        expressions.Add(new GameExpression("-", " restado con "));
        expressions.Add(new GameExpression("*", " multiplicado con "));
        expressions.Add(new GameExpression("/", " dividido con "));
        expressions.Add(new GameExpression("^", " elevado a la "));
        expressions.Add(new GameExpression("(", "("));
        expressions.Add(new GameExpression(")", ")"));

        return expressions;
    }

    //obtener el número de cuadros marcados de un tablero
    private int numberCheckTable(Table table)
    {
        int result = 0;
        foreach(int i in table.table)
        {
            if (i == 1) result++;
        }

        return result;
    }
}
