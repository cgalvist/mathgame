﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DropZone : MonoBehaviour, IDropHandler
{

    public void OnDrop(PointerEventData eventData)
    {
        //Debug.Log(eventData.pointerDrag.name + " was dropped on " + gameObject.name);

        /*
         Draggable d = eventData.pointerDrag.GetComponent<Draggable>();
        if (d != null)
        {
            d.parentToReturnTo = this.transform;
        }
         */

        GameObject dragObject = eventData.pointerDrag.transform.gameObject;
        if (dragObject != null)
        {
            //eliminar etiquetas anteriormente seleccionadas al hacer drag & drop
            Transform[] containers = dragObject.GetComponentsInChildren<Transform>();

            foreach (Transform child in containers)
            {
                if (child.name == "labelContainer")
                {
                    Destroy(child.gameObject);
                }
            }

            //agregar etiqueta con numero seleccionado en drag & drop
            GameObject labelContainer = new GameObject();
            GameObject label = new GameObject();

            labelContainer.AddComponent<RectTransform>();
            labelContainer.name = "labelContainer";

            label.AddComponent<Text>();
            label.name = gameObject.name;

            Text labelText = label.GetComponent<Text>();

            labelText.text = gameObject.name;
            labelText.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
            labelText.resizeTextForBestFit = true;
            labelText.alignment = TextAnchor.MiddleCenter;
            labelText.color = new Color32(0, 0, 255, 255);
            
            label.transform.SetParent(labelContainer.transform);
            labelContainer.transform.SetParent(dragObject.transform);
        }
    }
}
