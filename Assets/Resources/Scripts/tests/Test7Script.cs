﻿using UnityEngine;
using Assets.Resources.Scripts;
using UnityEngine.UI;

public class Test7Script : MonoBehaviour
{

    //nivel de dificultad del juego
    private int level = 0;

    //datos del nivel
    private LevelT7 levelInfo;

    //datos de la camara
    private bool camAvailable;
    private WebCamTexture backCam;
    public RawImage background;
    public AspectRatioFitter fit;

    //pop ups del nivel
    public GameObject popupSuccess;
    public GameObject popupMessage;
    public Text popupText;
    public GameObject popupFigures;
    public GameObject popupCamera;

    //objetos del nivel
    public Text levelText;
    public GameObject boardList;
    public GameObject board;
    public GameObject polygon;
    public GameObject figuresGrid;
    public GameObject selectedFigure;
    public Image colorSelected;

    // Use this for initialization
    void Start ()
    {
        //verificar nivel de dificultad del juego
        //PlayerPrefs.SetInt ("test7_level", 0);
        if (PlayerPrefs.GetInt("test7_level") >= 0)
        {
            level = PlayerPrefs.GetInt("test7_level");
        }
        else
        {
            PlayerPrefs.SetInt("test7_level", 0);
        }
        //print("Nivel actual: " + level);

        //inicializar datos de los niveles del test
        GeneralScript.initLevelsT7();
        levelInfo = (LevelT7)GeneralScript.levelsT7.levels[level];

        //inicializar datos
        levelText.text = levelInfo.text;
        initFiguresColors(levelInfo.figures, boardList, board, polygon);
        StartCoroutine(GeneralScript.resizeCells(boardList, "SuccessionsGrid"));
        initFiguresGrid();
        openDeviceCamera();

    }

    private void Update()
    {
        if (!camAvailable)
            return;

        float ratio = (float)backCam.width / (float)backCam.height;
        fit.aspectRatio = ratio;

        float scaleY = backCam.videoVerticallyMirrored ? -1f : 1f;
        background.rectTransform.localScale = new Vector3(1f, scaleY, 1f);
    }

    //inicializar figuras con colores
    public void initFiguresColors(FigureT7[] figures, GameObject boardList, GameObject board, GameObject figure)
    {
        Color myColor;

        for (int i = 0; i < figures.Length; i++)
        {
            GameObject containerClone = Instantiate(board) as GameObject;
            containerClone.transform.SetParent(boardList.transform);

            GameObject figureClone = Instantiate(figure) as GameObject;
            figureClone.transform.SetParent(containerClone.transform);

            Image imageFigureClone = figureClone.GetComponent<Image>();
            imageFigureClone.sprite = Resources.Load<Sprite>("Images/polygons/" + figures[i].figure);

            //cambiar color de la figura
            myColor = new Color();
            ColorUtility.TryParseHtmlString(figures[i].color, out myColor);
            imageFigureClone.color = myColor;
        }
    }

    //inicializar lista de figuras
    public void initFiguresGrid()
    {
        //redimensionar malla de figuras
        GameObject parent = figuresGrid.transform.parent.transform.parent.gameObject;
        RectTransform parentRect = parent.GetComponent<RectTransform>();

        float parentXSize = parentRect.rect.width, parentYSize = parentRect.rect.height;
        //print("parent width and height: " + parentRect.rect.width + ", " + parentRect.rect.height);
        float pieceXSize = parentXSize / 3, pieceYSize = parentYSize / 3;

        GridLayoutGroup figuresGridLayout = figuresGrid.GetComponent<GridLayoutGroup>();
        figuresGridLayout.cellSize = new Vector2(pieceXSize, pieceYSize);

        //agregar figuras a la lista
        Sprite[] figures = Resources.LoadAll<Sprite>("Images/polygons/");
        GameObject figureClone;
        foreach (Sprite figure in figures)
        {
            //cambiar imagen
            figureClone = Instantiate(polygon) as GameObject;
            figureClone.GetComponent<Image>().sprite = figure;
            string tempName = figure.name;

            //agregar evento de click
            figureClone.AddComponent<Button>();
            figureClone.GetComponent<Button>().onClick.AddListener(delegate { selectFigure(tempName); });

            //agregar a la lista
            figureClone.transform.SetParent(figuresGrid.transform);
        }
    }

    //seleccionar figura desde pop up
    public void selectFigure()
    {
        popupFigures.SetActive(true);
    }

    //accion al seleccionar una figura del pop up
    public void selectFigure(string name)
    {
        selectedFigure.GetComponent<Image>().sprite = Resources.Load<Sprite>("Images/polygons/" + name);
        popupFigures.SetActive(false);
    }

    //limpiar cambios realizados por el usuario
    public void clear()
    {
        selectedFigure.GetComponent<Image>().sprite = Resources.Load<Sprite>("Images/polygons/3");
    }

    //abrir pop up para seleccionar color con la camara
    public void selectColor()
    {
        popupCamera.SetActive(true);

        if (!camAvailable)
            return;

        backCam.Play();
        background.texture = backCam;
    }

    //iniciar popup de camara del dispositivo
    public void openDeviceCamera()
    {
        WebCamDevice[] devices = WebCamTexture.devices;

        if(devices.Length == 0)
        {
            Debug.Log("no se detectaron camaras");
            camAvailable = false;
            return;
        }

        for(int i = 0; i < devices.Length; i++)
        {
            if (!devices[i].isFrontFacing)
            {
                backCam = new WebCamTexture(devices[i].name,Screen.width,Screen.height);
            }
        }

        if(backCam == null)
        {
            Debug.Log("no se detecto camara trasera");
            return;
        }

        camAvailable = true;
    }

    //capturar una imagen de la camara y obtener un color promedio
    public void takePhoto()
    {
        if (!camAvailable)
            return;

        Color[] matrixImage = backCam.GetPixels();
        backCam.Stop();

        float r = 0f, g = 0f, b = 0f;

        foreach (Color color in matrixImage) {
            r += color.r;
            g += color.g;
            b += color.b;
        }

        r = r / matrixImage.Length;
        g = g / matrixImage.Length;
        b = b / matrixImage.Length;

        Color result = new Color(r,g,b);

        //aproximar el color capturado por uno mas especifico
        //[yellow, cyan, magenta, red, green, blue, black, white]
        result.r = (result.r > 0.5f) ? 1.0f : 0.0f;
        result.g = (result.g > 0.5f) ? 1.0f : 0.0f;
        result.b = (result.b > 0.5f) ? 1.0f : 0.0f;

        popupCamera.SetActive(false);
        
        //cambiar el color del boton de seleccion
        colorSelected.color = result;
    }

    //validar respuesta del usuario
    public void send()
    {
        //print(selectedFigure.GetComponent<Image>().sprite.name);
        //print(ColorUtility.ToHtmlStringRGB(colorSelected.color));

        bool correctAnswer = true;
        if (!(selectedFigure.GetComponent<Image>().sprite.name == levelInfo.answer.figure.ToString()
            && ("#" + ColorUtility.ToHtmlStringRGB(colorSelected.color) == levelInfo.answer.color)))
        {
            correctAnswer = false;
        }

        // datos de usuario
        string usernameStr = PlayerPrefs.GetString("username");
        string json = "{\"username\":\"" + usernameStr + "\"," +
            "\"problem\":\"" + "test7 - level " + level + "\"," +
            "\"score\":\"" + (correctAnswer ? 1 : 0) + "\"}";
        var formData = GeneralScript.castJsonToByte(json);
        Debug.Log("json: " + json);

        // enviar datos al back end
        WWW www = new WWW(GeneralScript.backEndUrl + "score", formData, GeneralScript.getPostHeader());
        StartCoroutine(GeneralScript.waitForRequest(www));

        if (correctAnswer)
        {
            level++;

            if (level < GeneralScript.levelsT7.levels.Length)
            {
                PlayerPrefs.SetInt("test7_level", level);
                popupSuccess.SetActive(true);
            }
            else
            {
                popupText.text = "Felicitaciones, ya has superado todos los niveles de esta prueba.";
                popupMessage.SetActive(true);
            }
        }
        else
        {
            popupText.text = "Respuesta erronea. Intenta de nuevo.";
            popupMessage.SetActive(true);
        }
    }
}
