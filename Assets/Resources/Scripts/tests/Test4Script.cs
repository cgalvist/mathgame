﻿using UnityEngine;
using System.Collections;
using Assets.Resources.Scripts.Model;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Test4Script : MonoBehaviour {

    public static ArrayList Niveles;
    public static int nivelActual;
    public Text testText;
    public Text errorText;
    public Text correctText;
    public Button siguiente;
    public Button reintentar;
    public Button menu;
    public static string estado;

    void Start () {
        nivelActual = 0;

        Niveles = new ArrayList();
        Niveles.Add(new EjercicioT4("Nivel 1 - ¿Qué figura continúa la serie?", 3));
        Niveles.Add(new EjercicioT4("Nivel 2 - ¿Qué figura continúa la serie?", 1));
        Niveles.Add(new EjercicioT4("Nivel 3 - ¿Qué figura continúa la serie?", 2));
        Niveles.Add(new EjercicioT4("Nivel 4 - ¿Qué figura continúa la serie?", 2));
        Niveles.Add(new EjercicioT4("Nivel 5 - ¿Qué figura continúa la serie?", 2));
        Niveles.Add(new EjercicioT4("Nivel 6 - ¿Qué figura continúa la serie?", 1));
        Niveles.Add(new EjercicioT4("Nivel 7 - ¿Qué figura no sigue la lógica de la serie?", 5));
        Niveles.Add(new EjercicioT4("Nivel 8 - ¿Qué figura no sigue la lógica de la serie?", 1));
        Niveles.Add(new EjercicioT4("Nivel 9 - ¿Cuál es el dibujo más diferente?", 3));
        Niveles.Add(new EjercicioT4("Nivel 10 - ¿Cuál es el dibujo más diferente?", 3));

        iniciar();

    }

    public void iniciar()
    {
        errorText.gameObject.SetActive(false);
        correctText.gameObject.SetActive(false);
        siguiente.gameObject.SetActive(false);
        reintentar.gameObject.SetActive(false);
        menu.gameObject.SetActive(false);

        testText.text = ((EjercicioT4)Niveles[0]).Texto;
        estado = "inGame";
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene("category1");
        }
    }

}
