﻿using UnityEngine;
using Assets.Resources.Scripts;
using System.Collections;
using UnityEngine.UI;
using System;

public class Test5Script : MonoBehaviour {
    
    //nivel de dificultad del juego
    private int level = 0;

    //datos del nivel
    private LevelT5 levelInfo;

    //pop ups del nivel
    public GameObject popupSuccess;
    public GameObject popupMessage;
    public Text popupText;

    //objetos del nivel
    public GameObject boardList;
    public GameObject board;
    public GameObject polygon;
    public GameObject polygonList;
    public GameObject container;
    public GameObject positionList;
    public GameObject position;

    // Use this for initialization
    void Start()
    {
        //verificar nivel de dificultad del juego
        //PlayerPrefs.SetInt ("test5_level", 0);
        if (PlayerPrefs.GetInt("test5_level") >= 0)
        {
            level = PlayerPrefs.GetInt("test5_level");
        }
        else
        {
            PlayerPrefs.SetInt("test5_level", 0);
        }
        //print("Nivel actual: " + level);

        //inicializar datos de los niveles del test
        GeneralScript.initLevelsT5();
        levelInfo = (LevelT5)GeneralScript.levelsT5.levels[level];

        //inicializar datos
        GeneralScript.initFigures(levelInfo.figures, boardList, board, polygon);
        StartCoroutine(GeneralScript.resizeCells(boardList, "SuccessionsGrid"));
        GeneralScript.initFigures2(levelInfo.answers, polygonList, container, polygon);
        GeneralScript.initPositions(levelInfo.range, positionList, container, position);
    }

    //limpiar cambios realizados por el usuario
    public void clear()
    {
        Transform[] containers = polygonList.GetComponentsInChildren<Transform>();

        foreach (Transform child in containers)
        {
            if (child.name == "labelContainer")
            {
                Destroy(child.gameObject);
            }
        }
    }

    //validar respuesta del usuario
    public void send()
    {
        Transform[] containers = polygonList.GetComponentsInChildren<Transform>();
        
        ArrayList figures = new ArrayList(), answers = new ArrayList();
        foreach (Transform child in containers)
        {
            if (child.parent.name == "PolygonsList")
            {
                figures.Add(child.name);
            }
            else
            {
                if (child.name == "labelContainer")
                {
                    Transform label = child.GetComponentsInChildren<Transform>()[1];
                    answers.Add(label.name);
                }
            }
            
        }

        Boolean correctAnswer = true;
        int index;

        if (figures.Count == answers.Count)
        {
            foreach (Answer answer in levelInfo.answers)
            {
                index = answers.IndexOf(answer.position.ToString());
                if (index >= 0)
                {
                    if(!(answer.figure.ToString() == figures[index] as string
                        && answer.position.ToString() == answers[index] as string))
                    {
                        correctAnswer = false;
                        break;
                    }
                }
                else
                {
                    correctAnswer = false;
                    break;
                }
            }
        }
        else
        {
            correctAnswer = false;
        }

        // datos de usuario
        string usernameStr = PlayerPrefs.GetString("username");
        string json = "{\"username\":\"" + usernameStr + "\"," +
            "\"problem\":\"" + "test5 - level " + level + "\"," +
            "\"score\":\"" + (correctAnswer ? 1 : 0) + "\"}";
        var formData = GeneralScript.castJsonToByte(json);
        Debug.Log("json: " + json);

        // enviar datos al back end
        WWW www = new WWW(GeneralScript.backEndUrl + "score", formData, GeneralScript.getPostHeader());
        StartCoroutine(GeneralScript.waitForRequest(www));

        if (correctAnswer)
        {
            level++;

            if (level < GeneralScript.levelsT5.levels.Length)
            {
                PlayerPrefs.SetInt("test5_level", level);
                popupSuccess.SetActive(true);
            }
            else
            {
                popupText.text = "Felicitaciones, ya has superado todos los niveles de esta prueba.";
                popupMessage.SetActive(true);
            }
        }
        else
        {
            popupText.text = "Respuesta erronea. Intenta de nuevo.";
            popupMessage.SetActive(true);
        }
    }
}
