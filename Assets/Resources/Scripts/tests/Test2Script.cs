﻿using UnityEngine;
using System.Collections;
using Assets.Resources.Scripts;
using UnityEngine.UI;

public class Test2Script : MonoBehaviour {

    public static ArrayList Niveles;
    public static ArrayList Respuestas;
    public static int nivelCont;
    public static int respuesta;

    public TextMesh Respuesta1;
    public TextMesh Respuesta2;
    public TextMesh Respuesta3;
    public TextMesh Respuesta4;
    public TextMesh Respuesta5;
    public TextMesh Respuesta6;
    public Text Titulo;
    public Button menu;
    public Button siguiente;
    public Button reiniciar;


    // Use this for initialization
    void Start () {
        Niveles = new ArrayList();
        Niveles.Add(new EjercicioT2("Nivel 1 - ¿Cuál es el valor de X en: 4, 7, 10, 13, X?", 16));
        Niveles.Add(new EjercicioT2("Nivel 2 - ¿Qué número continúa en: 42, 38, 34, 30…?", 26));
        Niveles.Add(new EjercicioT2("Nivel 3 - ¿Cuál es el valor de X en: 8, 16, 32, 64, X?", 128));
        Niveles.Add(new EjercicioT2("Nivel 4 - ¿Qué número continúa en: 80, 40, 20, 10…?", 5));
        Niveles.Add(new EjercicioT2("Nivel 5 - ¿Cuál es el valor de X en: 8, 9, 11, 14, 18, X?", 23));
        Niveles.Add(new EjercicioT2("Nivel 6 - ¿Cuál es el valor de X en: 8, 10, 13, 17, 22, X?", 28));
        Niveles.Add(new EjercicioT2("Nivel 7 - ¿Cuál es el valor de X en: 4, 8, 10, 20, 22, 44, X?", 46));
        Niveles.Add(new EjercicioT2("Nivel 8 - ¿Qué número continúa en: 1, 4, 9, 16, 25…?", 36));
        Niveles.Add(new EjercicioT2("Nivel 9 - ¿Qué número continúa en: 4, 4, 8, 24, 96…?", 480));
        Niveles.Add(new EjercicioT2("Nivel 10 - ¿Cuál es el valor de X en: 8, 13, 23, 38, 58, X?", 83));

        menu.gameObject.SetActive(false);
        siguiente.gameObject.SetActive(false);
        reiniciar.gameObject.SetActive(false);

        Respuestas = new ArrayList();
        Respuestas.Add(Respuesta1);
        Respuestas.Add(Respuesta2);
        Respuestas.Add(Respuesta3);
        Respuestas.Add(Respuesta4);
        Respuestas.Add(Respuesta5);
        Respuestas.Add(Respuesta6);

        iniciar(Titulo,0);
        
    }

    public static void iniciar(Text Titulo, int nivCount)
    {
        nivelCont = nivCount;
        respuesta = ((EjercicioT2)Niveles[nivelCont]).Respuesta;
        ArrayList numIndices = generarNumerosIndices();
        ArrayList numRespuestas = generarNumerosRespuesta(respuesta);

        Titulo.text = ((EjercicioT2)Niveles[nivelCont]).Texto.ToString();

        foreach (int indice in numIndices)
        {
            ((TextMesh)Respuestas[indice]).text = numRespuestas[indice].ToString();
        }
    }
	
	// Update is called once per frame
	void Update () {
        
    }

    public static ArrayList generarNumerosIndices()
    {
        int valor;
        ArrayList resultado = new ArrayList();
        while (resultado.Count < 6)
        {
            valor = Random.Range(0, 6);
            if (!resultado.Contains(valor))
            {
                resultado.Add(valor);
            }
        }
        return resultado;
    }

    public static ArrayList generarNumerosRespuesta(int pivote)
    {
        int valor;
        ArrayList resultado = new ArrayList();
        while (resultado.Count<6)
        {
            valor = Random.Range(pivote - 10, pivote + 10 + 1);
            if (!resultado.Contains(valor))
            {
                resultado.Add(valor);
            }
        }
        valor = Random.Range(0, 6);
        if (!resultado.Contains(pivote))
        {
            resultado[valor] = pivote;
        }
        return resultado;
    }
}
