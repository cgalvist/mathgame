﻿using UnityEngine;
using Assets.Resources.Scripts;
using UnityEngine.UI;
using System.Linq;
using System.Collections;
using System;

public class Test6Script : MonoBehaviour
{

    //nivel de dificultad del juego
    private int level = 0;

    //datos del nivel
    private LevelT6 levelInfo;

    //pop ups del nivel
    public GameObject popupSuccess;
    public GameObject popupMessage;
    public Text popupText;

    //objetos del nivel
    public GameObject boardList;
    public GameObject board;
    public GameObject polygon;
    public GameObject piecesList;
    public GameObject answerList;
    public GameObject piece;

    // Use this for initialization
    void Start ()
    {
        //verificar nivel de dificultad del juego
        //PlayerPrefs.SetInt ("test6_level", 0);
        if (PlayerPrefs.GetInt("test6_level") >= 0)
        {
            level = PlayerPrefs.GetInt("test6_level");
        }
        else
        {
            PlayerPrefs.SetInt("test6_level", 0);
        }
        //print("Nivel actual: " + level);

        //inicializar datos de los niveles del test
        GeneralScript.initLevelsT6();
        levelInfo = (LevelT6)GeneralScript.levelsT6.levels[level];

        //inicializar datos
        GeneralScript.initFigures(levelInfo.figures, boardList, board, polygon);
        StartCoroutine(GeneralScript.resizeCells(boardList, "SuccessionsGrid"));
        resizePieces();
        initPieces(levelInfo, piecesList, piece);
    }

    //inicializar piezas de la frase
    public void initPieces(LevelT6 levelInfo, GameObject piecesList, GameObject piece)
    {
        string[] allPieces = new string[levelInfo.answer.Length + levelInfo.options.Length];
        levelInfo.answer.CopyTo(allPieces, 0);
        levelInfo.options.CopyTo(allPieces, levelInfo.answer.Length);

        //cambiar orden de las preguntas de manera aleatoria
        System.Random rnd = new System.Random();
        string[] randomAllPieces = allPieces.OrderBy(x => rnd.Next()).ToArray();

        //agregar todos los strings al juego
        foreach (string tempPiece in randomAllPieces)
        {
            GameObject containerClone = Instantiate(piece) as GameObject;
            Button buttonPiece = containerClone.GetComponent<Button>();
            buttonPiece.onClick.AddListener(delegate { checkCell(containerClone); });

            Text[] textPiece = containerClone.GetComponentsInChildren<Text>();
            textPiece[0].text = tempPiece;

            containerClone.transform.SetParent(piecesList.transform);
        }
    }

    //redimensionar piezas en las dos tablas del juego
    public void resizePieces()
    {
        GridLayoutGroup gridPieces = piecesList.GetComponent<GridLayoutGroup>();
        GridLayoutGroup gridAnswer = answerList.GetComponent<GridLayoutGroup>();

        GameObject parent = piecesList.transform.parent.transform.parent.gameObject;
        RectTransform parentRect = parent.GetComponent<RectTransform>();

        float parentXSize = parentRect.rect.width, parentYSize = parentRect.rect.height;
        //print("parent width and height: " + parentRect.rect.width + ", " + parentRect.rect.height);

        float pieceXSize = parentXSize / 5, pieceYSize = parentYSize / 4;

        //print("piece width and height: " + parentXSize + ", " + parentYSize);

        gridPieces.cellSize = new Vector2(pieceXSize,pieceYSize);
        gridAnswer.cellSize = new Vector2(pieceXSize, pieceYSize);
    }

    //funcion al tocar una pieza de la frase
    public void checkCell(GameObject cellClone)
    {
        cellClone.transform.SetParent(
            (cellClone.transform.parent.transform == piecesList.transform) ? answerList.transform : piecesList.transform
            );
    }

    //limpiar cambios realizados por el usuario
    public void clear()
    {
        Transform[] containers = piecesList.GetComponentsInChildren<Transform>();

        foreach (Transform child in containers)
        {
            if (child.parent.gameObject == piecesList)
            {
                Destroy(child.gameObject);
            }
        }

        containers = answerList.GetComponentsInChildren<Transform>();

        foreach (Transform child in containers)
        {
            if (child.parent.gameObject == answerList)
            {
                Destroy(child.gameObject);
            }
        }

        initPieces(levelInfo, piecesList, piece);
    }

    //validar respuesta del usuario
    public void send()
    {
        Transform[] containers = answerList.GetComponentsInChildren<Transform>();
        ArrayList result = new ArrayList();

        //guardar resultados en un arraylist
        foreach (Transform child in containers)
        {
            if (child.parent.gameObject == answerList)
            {
                Text tempText = child.GetComponentsInChildren<Text>()[0];
                result.Add(tempText.text);
            }
        }

        Boolean correctAnswer = true;

        if(!result.ToArray().SequenceEqual(levelInfo.answer))
        {
            correctAnswer = false;
        }

        // datos de usuario
        string usernameStr = PlayerPrefs.GetString("username");
        string json = "{\"username\":\"" + usernameStr + "\"," +
            "\"problem\":\"" + "test6 - level " + level + "\"," +
            "\"score\":\"" + (correctAnswer ? 1 : 0) + "\"}";
        var formData = GeneralScript.castJsonToByte(json);
        Debug.Log("json: " + json);

        // enviar datos al back end
        WWW www = new WWW(GeneralScript.backEndUrl + "score", formData, GeneralScript.getPostHeader());
        StartCoroutine(GeneralScript.waitForRequest(www));

        if (correctAnswer)
        {
            level++;

            if (level < GeneralScript.levelsT6.levels.Length)
            {
                PlayerPrefs.SetInt("test6_level", level);
                popupSuccess.SetActive(true);
            }
            else
            {
                popupText.text = "Felicitaciones, ya has superado todos los niveles de esta prueba.";
                popupMessage.SetActive(true);
            }
        }
        else
        {
            popupText.text = "Respuesta erronea. Intenta de nuevo.";
            popupMessage.SetActive(true);
        }
    }
}
